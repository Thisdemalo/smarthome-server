package smarthome.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartHomeApiApp {
    public static void main(String[] args)
    {
        SpringApplication.run(SmartHomeApiApp.class, args);
    }
}
